from django.urls import path
from .views import index, add_status, delete_all_status

urlpatterns = [
    path('', index, name="homepage"),
    path('add_status/', add_status, name='add_status'),
    path('delete_all_status/', delete_all_status, name='delete_all_status'),
]
