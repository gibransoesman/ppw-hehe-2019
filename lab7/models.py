from django.db import models

class Status(models.Model):
    dateTime = models.DateTimeField()
    status = models.CharField(max_length = 300)
