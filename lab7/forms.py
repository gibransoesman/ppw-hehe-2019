from django import forms

class statusForm(forms.Form):
    attrs = {'class':'form-control'}
    status = forms.CharField(label='Status', max_length=300, required=True,widget=forms.TextInput(attrs=attrs))